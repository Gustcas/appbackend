package alticor.ventas.backEnd.com;

import static java.lang.Math.log;
import java.net.InetAddress;
import java.net.UnknownHostException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

@SpringBootApplication
public class AlticorVentasBackEndApplication {
    private static final Logger log = LoggerFactory.getLogger(AlticorVentasBackEndApplication.class);
        public static Environment env;
	public static void main(String[] args) throws UnknownHostException {	
                SpringApplication app = new SpringApplication(AlticorVentasBackEndApplication.class);
                env = app.run(args).getEnvironment();
                logApplicationStartup(env);
	}
    private static void logApplicationStartup(Environment env) throws UnknownHostException {
        String protocol = "http";
        if (env.getProperty("server.ssl.key-store") != null) {
            protocol = "https";
        }
        String serverPort = env.getProperty("server.port");
        String hostAddress = "localhost";
        log.info("\n----------------------------------------------------------\n\t"
                + "Application '{}' is running! Access URLs:\n\t"
                + "Local: \t\t{}://localhost:{}{}\n\t"
                + "Profile(s)_: \t{}\n----------------------------------------------------------",
                env.getProperty("spring.application.name"),
                protocol,
                serverPort,      
                protocol,
                hostAddress,
                serverPort,
                env.getActiveProfiles());
    }
}
