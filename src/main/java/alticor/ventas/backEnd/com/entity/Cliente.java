package alticor.ventas.backEnd.com.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "cliente")
public class Cliente implements Serializable {

    @Id
    @Column(name = "idCliente")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idCliente;

    @Column(name = "nombreCliente")
    private String nombreCliente;

    @Column(name = "apellidoCliente")
    private String apellidoCliente;

    @Column(name = "identificacionCliente")
    private String identificacionCliente;

    @Column(name = "direccionCliente")
    private String direccionCliente;
    
    @Column(name = "telefonoCliente")
    private String telefonoCliente;

    @Column(name = "estadoCliente")
    private char estadoCliente;
    
    @OneToMany(mappedBy = "idCliente")
    private List<OrdenCabecera> OrdenCabeceraList;

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getApellidoCliente() {
        return apellidoCliente;
    }

    public void setApellidoCliente(String apellidoCliente) {
        this.apellidoCliente = apellidoCliente;
    }

    public String getIdentificacionCliente() {
        return identificacionCliente;
    }

    public void setIdentificacionCliente(String identificacionCliente) {
        this.identificacionCliente = identificacionCliente;
    }

    public String getDireccionCliente() {
        return direccionCliente;
    }

    public void setDireccionCliente(String direccionCliente) {
        this.direccionCliente = direccionCliente;
    }

    public String getTelefonoCliente() {
        return telefonoCliente;
    }

    public void setTelefonoCliente(String telefonoCliente) {
        this.telefonoCliente = telefonoCliente;
    }

    public char getEstadoCliente() {
        return estadoCliente;
    }

    public void setEstadoCliente(char estadoCliente) {
        this.estadoCliente = estadoCliente;
    }

    public List<OrdenCabecera> getOrdenCabeceraList() {
        return OrdenCabeceraList;
    }

    public void setOrdenCabeceraList(List<OrdenCabecera> OrdenCabeceraList) {
        this.OrdenCabeceraList = OrdenCabeceraList;
    }

    @Override
    public String toString() {
        return "Cliente{" + "idCliente=" + idCliente + ", nombreCliente=" + nombreCliente + ", apellidoCliente=" + apellidoCliente + ", identificacionCliente=" + identificacionCliente + ", direccionCliente=" + direccionCliente + ", telefonoCliente=" + telefonoCliente + ", estadoCliente=" + estadoCliente + ", OrdenCabeceraList=" + OrdenCabeceraList + '}';
    }
    
    
}
