package alticor.ventas.backEnd.com.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "ordencabecera")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class OrdenCabecera implements Serializable {

    @Id
    @Column(name = "idorden")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idorden;

    @Column(name = "fechacreacionorden")
    private String fechacreacionorden;

    @Column(name = "estado")
    private char estado;

    @JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"}, allowSetters = true)
    @OneToMany(targetEntity = OrdenDetalle.class, cascade = CascadeType.PERSIST, fetch = FetchType.LAZY, mappedBy = "idorden")
    private List<OrdenDetalle> OrdenDetalleList;

    @Column(name = "idCliente")
    private int idCliente;
    
    @Column(name = "order_price_total")
    private Double orderPriceTotal;


    public int getIdorden() {
        return idorden;
    }

    public void setIdorden(int idorden) {
        this.idorden = idorden;
    }

    public String getFechacreacionorden() {
        return fechacreacionorden;
    }

    public void setFechacreacionorden(String fechacreacionorden) {
        this.fechacreacionorden = fechacreacionorden;
    }

    public char getEstado() {
        return estado;
    }

    public void setEstado(char estado) {
        this.estado = estado;
    }

    public List<OrdenDetalle> getOrdenDetalleList() {
        return OrdenDetalleList;
    }

    public void setOrdenDetalleList(List<OrdenDetalle> OrdenDetalleList) {
        this.OrdenDetalleList = OrdenDetalleList;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public Double getOrderPriceTotal() {
        return orderPriceTotal;
    }

    public void setOrderPriceTotal(Double orderPriceTotal) {
        this.orderPriceTotal = orderPriceTotal;
    }

    @Override
    public String toString() {
        return "OrdenCabecera{" + "idorden=" + idorden + ", fechacreacionorden=" + fechacreacionorden + ", estado=" + estado + ", OrdenDetalleList=" + OrdenDetalleList + ", idCliente=" + idCliente + ", orderPriceTotal=" + orderPriceTotal + '}';
    }

 
  

}
