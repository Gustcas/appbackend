
package alticor.ventas.backEnd.com.dto;

/**
 *
 * @author gustavo
 */

public class ClienteDto {
    private int idCliente;
    private String nombreCliente;
    private String apellidoCliente;
    private String identificacionCliente;
    private String direccionCliente;
    private String telefonoCliente;
    private char estadoCliente;  

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getApellidoCliente() {
        return apellidoCliente;
    }

    public void setApellidoCliente(String apellidoCliente) {
        this.apellidoCliente = apellidoCliente;
    }

    public String getIdentificacionCliente() {
        return identificacionCliente;
    }

    public void setIdentificacionCliente(String identificacionCliente) {
        this.identificacionCliente = identificacionCliente;
    }

    public String getDireccionCliente() {
        return direccionCliente;
    }

    public void setDireccionCliente(String direccionCliente) {
        this.direccionCliente = direccionCliente;
    }

    public String getTelefonoCliente() {
        return telefonoCliente;
    }

    public void setTelefonoCliente(String telefonoCliente) {
        this.telefonoCliente = telefonoCliente;
    }

    public char getEstadoCliente() {
        return estadoCliente;
    }

    public void setEstadoCliente(char estadoCliente) {
        this.estadoCliente = estadoCliente;
    }
    
}
