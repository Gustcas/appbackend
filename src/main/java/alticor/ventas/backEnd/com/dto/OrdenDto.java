package alticor.ventas.backEnd.com.dto;

import alticor.ventas.backEnd.com.entity.OrdenDetalle;
import java.util.List;

/**
 *
 * @author gustavo
 */
public class OrdenDto {
    private int idorden;
    private String fechacreacionorden;
    private char estado;
    private List<OrdenDetalle> OrdenDetalleList;
    private int idCliente; 
    private Double orderPriceTotal;

    public int getIdorden() {
        return idorden;
    }

    public void setIdorden(int idorden) {
        this.idorden = idorden;
    }

    public String getFechacreacionorden() {
        return fechacreacionorden;
    }

    public void setFechacreacionorden(String fechacreacionorden) {
        this.fechacreacionorden = fechacreacionorden;
    }

    public char getEstado() {
        return estado;
    }

    public void setEstado(char estado) {
        this.estado = estado;
    }

    public List<OrdenDetalle> getOrdenDetalleList() {
        return OrdenDetalleList;
    }

    public void setOrdenDetalleList(List<OrdenDetalle> OrdenDetalleList) {
        this.OrdenDetalleList = OrdenDetalleList;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public Double getOrderPriceTotal() {
        return orderPriceTotal;
    }

    public void setOrderPriceTotal(Double orderPriceTotal) {
        this.orderPriceTotal = orderPriceTotal;
    }
}
