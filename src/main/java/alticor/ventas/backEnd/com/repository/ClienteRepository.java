package alticor.ventas.backEnd.com.repository;

import alticor.ventas.backEnd.com.entity.Cliente;
import alticor.ventas.backEnd.com.entity.Producto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author gustavo
 */
public interface ClienteRepository extends JpaRepository<Cliente,Integer>{    
    
    @Query("SELECT C FROM Cliente C WHERE C.identificacionCliente = :identificacionCliente ")
    public Cliente findClienteByCode(@Param("identificacionCliente") String identificacionCliente);
    
}
