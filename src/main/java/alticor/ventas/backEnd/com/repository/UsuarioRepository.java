package alticor.ventas.backEnd.com.repository;

import alticor.ventas.backEnd.com.entity.Usuario;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuarioRepository  extends JpaRepository<Usuario, Integer>{
    List<Usuario> findByUsuarioAndPassword(String usuario, String password);  
}
