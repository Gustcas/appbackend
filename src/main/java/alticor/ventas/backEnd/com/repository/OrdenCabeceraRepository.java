package alticor.ventas.backEnd.com.repository;

import alticor.ventas.backEnd.com.entity.OrdenCabecera;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrdenCabeceraRepository extends JpaRepository<OrdenCabecera,Integer>{
    
}
