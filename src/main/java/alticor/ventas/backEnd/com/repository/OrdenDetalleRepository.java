package alticor.ventas.backEnd.com.repository;

import alticor.ventas.backEnd.com.entity.OrdenDetalle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface OrdenDetalleRepository extends JpaRepository<OrdenDetalle, Integer> {

    @Query("SELECT I FROM OrdenDetalle I WHERE I.idproducto = :idproducto ")
    public OrdenDetalle findProductByCode(@Param("idproducto") Integer idproducto);
}
