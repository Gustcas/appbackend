package alticor.ventas.backEnd.com.repository;

import alticor.ventas.backEnd.com.entity.Producto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface ProductoRepository extends JpaRepository<Producto,Integer>{
    
    @Query("SELECT I FROM Producto I WHERE I.idproducto = :idproducto ")
    public Producto findProductByCode(@Param("idproducto") Integer idproducto);
}
