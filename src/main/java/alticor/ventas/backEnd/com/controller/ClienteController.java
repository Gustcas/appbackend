package alticor.ventas.backEnd.com.controller;

import alticor.ventas.backEnd.com.dto.ClienteDto;
import alticor.ventas.backEnd.com.entity.Cliente;
import alticor.ventas.backEnd.com.service.ClienteService;
import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;

/**
 *
 * @author gustavo
 */
@RestController
@RequestMapping("/api/v1/cliente")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
public class ClienteController {

    @Autowired
    private ClienteService _clienteService;

    @GetMapping("/listAllCliente")
    private List<Cliente> listAllCliente() {
        return _clienteService.getAllClientes();
    }

    @PostMapping("/findClienteByCode")
    private Cliente findClienteByCode(@RequestBody ClienteDto _clienteDto) {
        return _clienteService.findById(_clienteDto.getIdentificacionCliente());
    }

    @PostMapping("/saveCliente")
    private ResponseEntity<Cliente> saveCliente(@RequestBody Cliente _cliente) {
        Cliente clienteTemporality = _clienteService.create(_cliente);
        try {
            return ResponseEntity.created(new URI("/saveCliente" + clienteTemporality.getIdCliente()))
                    .body(clienteTemporality);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @DeleteMapping("/deleteById")
    private ResponseEntity<Void> deleteById(@RequestBody ClienteDto _clienteDto) {
        _clienteService.deleteById(_clienteDto.getIdCliente());
        return ResponseEntity.ok().build();
    }

}
