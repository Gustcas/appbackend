package alticor.ventas.backEnd.com.controller;

import alticor.ventas.backEnd.com.dto.ProductoDto;
import alticor.ventas.backEnd.com.entity.Producto;
import alticor.ventas.backEnd.com.service.ProductoService;
import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author gustavo
 */
@RestController
@RequestMapping("/api/v1/producto")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
public class ProductoController {

    @Autowired
    private ProductoService productService;

    @GetMapping("/listAllProducto")
    private List<Producto> listAllProducto() {
        return productService.getAllProductos();
    }

    @PostMapping("/findProductByCode")
    private Producto findProductByCode(@RequestBody ProductoDto _productoDto) {
        return productService.findById(_productoDto.getIdproducto());
    }

    @PostMapping("/saveProducto")
    private ResponseEntity<Producto> saveProducto(@RequestBody Producto product) {
        Producto productTemporality = productService.create(product);
        try {
            return ResponseEntity.created(new URI("/saveProducto" + productTemporality.getIdproducto()))
                    .body(productTemporality);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }
 
    @DeleteMapping(value = "deleteById/{idproducto}")
	private ResponseEntity<Void> deleteById(@PathVariable("idproducto") Integer idproducto) {
		productService.deleteById(idproducto);
		return ResponseEntity.ok().build();
	}
	


}
