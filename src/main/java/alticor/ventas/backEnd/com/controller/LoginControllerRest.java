package alticor.ventas.backEnd.com.controller;


import alticor.ventas.backEnd.com.service.UsuarioService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMethod;

@RestController
@RequestMapping("/api/v1")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})

public class LoginControllerRest {
	@Autowired
	private UsuarioService _usuarioService;
	@GetMapping(value = "/validarLogin/{usurio}/{password}")
	public boolean validarLogin(@PathVariable("usurio") String usurio, @PathVariable("password") String password)  {		
		boolean isValidarLOgin = _usuarioService.findCredentials(usurio,password);
		return isValidarLOgin;
	}	
	
}
