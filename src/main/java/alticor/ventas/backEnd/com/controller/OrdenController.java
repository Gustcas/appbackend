package alticor.ventas.backEnd.com.controller;

import alticor.ventas.backEnd.com.dto.OrdenDto;
import alticor.ventas.backEnd.com.dto.ProductoDto;
import alticor.ventas.backEnd.com.entity.OrdenCabecera;
import alticor.ventas.backEnd.com.service.OrdenService;
import java.net.URI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author gustavo
 */
@RestController
@RequestMapping("/api/v1/Orden")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST})
public class OrdenController {

    @Autowired
    private OrdenService _ordenService;

    @PostMapping("/saveOrd")
    private ResponseEntity<OrdenCabecera> saveOrd(@RequestBody OrdenCabecera _ordenCabecera) {
        System.out.println("OrdenController************************ " + _ordenCabecera);
        OrdenCabecera ordTemporality = _ordenService.createOrder(_ordenCabecera);
        try {
            return ResponseEntity.created(new URI("/saveOrd" + ordTemporality.getIdCliente()))
                    .body(ordTemporality);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @PostMapping("/isProductExisInOrderDitalle")
    private boolean isProductExisInOrderDitalle(@RequestBody ProductoDto _productoDto) {
        System.out.println("OrdenController************************ " + _productoDto.getIdproducto());
        return _ordenService.isProductExisInOrderDitalle(_productoDto);
    }

}
