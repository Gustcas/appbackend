package alticor.ventas.backEnd.com.service;

import alticor.ventas.backEnd.com.dto.ClienteDto;
import alticor.ventas.backEnd.com.entity.Cliente;
import alticor.ventas.backEnd.com.repository.ClienteRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author gustavo
 */
@Service
public class ClienteService {

    @Autowired
    private ClienteRepository _clienteRepository;

    public Cliente create(Cliente _cliente) {
        return _clienteRepository.save(_cliente);
    }

    public List<Cliente> getAllClientes() {
        return _clienteRepository.findAll();
    }

    public void deleteById(Integer pdtId) {
        _clienteRepository.deleteById(pdtId);
    }

    public Cliente findById(String identificacionCliente) {
        return _clienteRepository.findClienteByCode(identificacionCliente);
    }

}
