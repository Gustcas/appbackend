package alticor.ventas.backEnd.com.service;

import alticor.ventas.backEnd.com.entity.Producto;
import alticor.ventas.backEnd.com.repository.ProductoRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author gustavo
 */
@Service
public class ProductoService {

    @Autowired
    private ProductoRepository _productoRepository;

    public Producto create(Producto product) {
        return _productoRepository.save(product);
    }

    public List<Producto> getAllProductos() {
        return _productoRepository.findAll();
    }

    public void deleteById(Integer pdtId) {
        _productoRepository.deleteById(pdtId);
    }
    

    public Producto findById(int usrId) {
        return _productoRepository.findProductByCode(usrId);
    }

    public void updateProducto(Producto product, int pdtId) {
        Producto productOne = _productoRepository.findProductByCode(pdtId);
        productOne.setIdproducto(product.getIdproducto());
        productOne.setNombreproducto(product.getNombreproducto());
        productOne.setPrecioventa(product.getPrecioventa());
        productOne.setEstado(product.getEstado());
        productOne.setStock(product.getStock());
        _productoRepository.save(productOne);
    }

}
