package alticor.ventas.backEnd.com.service;

import alticor.ventas.backEnd.com.dto.OrdenDto;
import alticor.ventas.backEnd.com.dto.ProductoDto;
import alticor.ventas.backEnd.com.entity.OrdenCabecera;
import alticor.ventas.backEnd.com.entity.OrdenDetalle;
import alticor.ventas.backEnd.com.repository.OrdenCabeceraRepository;
import alticor.ventas.backEnd.com.repository.OrdenDetalleRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author gustavo
 */
@Service
public class OrdenService {

    @Autowired
    private OrdenCabeceraRepository _ordenCabeceraRepository;

    @Autowired
    private OrdenDetalleRepository _ordenDetalleRepository;

    public OrdenCabecera createOrder(OrdenCabecera _ordenCabecera) {
        System.out.println("OrdenService************************ " + _ordenCabecera);

        List<OrdenDetalle> details = _ordenCabecera.getOrdenDetalleList();
        _ordenCabecera.setOrdenDetalleList(null);
        _ordenCabeceraRepository.save(_ordenCabecera);
        for (OrdenDetalle det : details) {
            det.setIdorden(_ordenCabecera.getIdorden());
            double iva = det.getIva() * 0.12;
            det.setIva(iva);
            det.setEstado('A');
        }
        _ordenDetalleRepository.saveAll(details);
        _ordenCabecera.setOrdenDetalleList(details);
        return _ordenCabecera;
    }

    public boolean isProductExisInOrderDitalle(ProductoDto _productoDto) {
        boolean isExistProduct = false;
        try {
            OrdenDetalle _ordenDetalle = new OrdenDetalle();
            _ordenDetalle = _ordenDetalleRepository.findProductByCode(_productoDto.getIdproducto());
            System.out.println("OrdenController************************ " + _ordenDetalle != null);
            return isExistProduct = _ordenDetalle != null ? true : false;

        } catch (Exception e) {
        }
        return isExistProduct;
    }

}
