package alticor.ventas.backEnd.com.service;

import alticor.ventas.backEnd.com.entity.Usuario;
import alticor.ventas.backEnd.com.repository.UsuarioRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author gustavo
 */
@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    public Usuario create(Usuario user) {
        return usuarioRepository.save(user);
    }

    public List<Usuario> getAllUsuarios() {
        return usuarioRepository.findAll();
    }

    public void delete(Usuario user) {
        usuarioRepository.delete(user);
    }

    public Optional<Usuario> findById(int usrId) {
        return usuarioRepository.findById(usrId);
    }

    public Boolean findCredentials(String userName, String password) {
        boolean isLogin = false;
        List<Usuario> _listaUsuarioTmp = usuarioRepository.findByUsuarioAndPassword(userName, password);
        if (_listaUsuarioTmp.size() > 0) {
            isLogin = true;
        }
        return isLogin;
    }

}
